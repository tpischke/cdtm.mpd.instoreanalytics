package com.example.instoreanalytics3.app;

import android.graphics.PointF;
import android.util.Log;
import android.view.View;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Paint;
import android.util.AttributeSet;

import org.opencv.features2d.KeyPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

/**
 * Created by Vincent on 15.05.14.
 */
public class DrawOnTop extends View{

    List<PointF> pointList;
    List<Integer> IDList;
    int walkInCount = 0;
    int walkOutCount = 0;

        public DrawOnTop(Context context) {
            super(context);
            pointList = new ArrayList<PointF>();
            IDList = new ArrayList<Integer>();

            IDList.add(1);
            pointList.add(new PointF(20, 20));
            // TODO Auto-generated constructor stub
        }

        public DrawOnTop(Context context, AttributeSet attrs) {
            super(context, attrs);
            pointList = new ArrayList<PointF>();
            IDList = new ArrayList<Integer>();

            IDList.add(1);
            pointList.add(new PointF(20, 20));
        }

    public void setBlobsAndIDs(List<PointF> blobs, List<Integer> IDs){
        pointList = blobs;
        IDList = IDs;

        Iterator<PointF> iterator = pointList.iterator();

        while (iterator.hasNext()) {
            PointF testPoint = iterator.next();

            Log.d("DrawOnTop ",String.format("x coordinate: %f ", testPoint.x));
            Log.d("DrawOnTop ",String.format("y coordinate: %f ", testPoint.y));
        }

        this.invalidate();
    }

        @Override
        protected void onDraw(Canvas canvas) {
            // TODO Auto-generated method stub

            Paint paint = new Paint();
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(Color.GREEN);
            paint.setTextSize(16);

            canvas.drawLine(0, this.getHeight()/2, this.getWidth(), this.getHeight()/2, paint);
            canvas.drawText("Down: " + Integer.toString(walkOutCount), 3*this.getWidth()/4, (this.getHeight()/2)+16, paint);
            canvas.drawText("Up: " + Integer.toString(walkInCount), 3*this.getWidth()/4, (this.getHeight()/2)-5, paint);

            for (int i = 0; i<pointList.size(); i++){
                float x = pointList.get(i).x;
                float y = pointList.get(i).y;
                int ID = IDList.get(i);

                canvas.drawCircle(x, y, 10, paint);
                canvas.drawText(Integer.toString(ID), x+20, y+20, paint);
            }

            super.onDraw(canvas);
        }

    public void setCounters(int _walkInCount, int _walkOutCount) {
        this.walkInCount = _walkInCount;
        this.walkOutCount = _walkOutCount;
    }
}
