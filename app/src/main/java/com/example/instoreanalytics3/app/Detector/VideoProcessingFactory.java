package com.example.instoreanalytics3.app.Detector;

import android.app.Activity;
import android.graphics.PointF;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.appcompat.R;
import android.util.Log;
import android.widget.Toast;

import com.example.instoreanalytics3.app.DrawOnTop;
import com.example.instoreanalytics3.app.ServerInterface;

import org.json.JSONArray;
import org.json.JSONObject;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.KeyPoint;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.opencv.core.Core.minMaxLoc;

/**
 * Created by Vincent on 28.05.14.
 */
public class VideoProcessingFactory {
    int counter = 0;


    //Variable that stores the function of the camera. 0:heatmap, 1:peopleCounting, 2:shoppingWindowAnalysis
    int function = 2;

    //Variables that contain the current and last frame
    Mat currentFrame;
    Mat lastGrayFrame;
    Mat currentGrayFrame;

    //Counting Border
    double countingBorder = 0.50;

    //Camera picture dimensions
    double cameraHeight = 0.;
    double cameraWidth = 0.;

    //Epsilon for Thresholding
    private double epsilon = 20.0;

    //Variables for in-out-counting
    private int walkInCount = 0;
    private int walkOutCount = 0;

    //Variables for the blob-tracking algorithm
    private int currentBlobID;
    private List<Integer> blobIDs = new ArrayList<Integer>();


    //Matrix that contains the added activities
    Mat activityMap;

    //Lists that contain detected blobs of current and of last frame
    List<KeyPoint> oldBlobs = new ArrayList<KeyPoint>();
    List<KeyPoint> currentBlobs = new ArrayList<KeyPoint>();

    //Transparent Overlay to visualize tracking on-screen
    DrawOnTop mDraw;

    //OpenCV simple Blob detector for detection of customer heads
    FeatureDetector blobDetector;
    FaceDetector faceDetector;

    //This List will map the blobs
    List<KeyPoint> blobPairs;

    //Server connection
    ServerInterface serverInt = new ServerInterface();

    //Timer for the ativity map updates
    TimertestActivity activityMapTimer;

    Rect[] zones;

    //Phone Camera width:480, phone camera height: 640

    int zoneWidth = 40;
    int zoneHeight = 40;

    int zoneXCount = 16;
    int zoneYCount = 12;

    public VideoProcessingFactory(DrawOnTop _mDraw){
        blobDetector = FeatureDetector.create(FeatureDetector.SIMPLEBLOB);
        faceDetector = new FaceDetector();

        blobPairs = new ArrayList<KeyPoint>();
        currentBlobID = 0;
        mDraw = _mDraw;

        activityMapTimer = new TimertestActivity();

        initZones();
    }

    public void initZones(){
        zones = new Rect[zoneXCount * zoneYCount];

        for(int x = 0; x<zoneXCount; x++){
            for(int y = 0; y<zoneYCount; y++){
                zones[x+y*zoneXCount] = new Rect(x*zoneWidth, y*zoneHeight, zoneWidth, zoneHeight);
            }
        }

        zones[zoneXCount *zoneYCount - 1] = new Rect((zoneXCount-1)*zoneWidth, (zoneYCount-1)*zoneHeight, zoneWidth -1, zoneHeight -1);
    }

    public void processFrame(Mat frame){
        currentFrame = frame;

        //Create differential frame
        Mat diffFrame = new Mat(currentFrame.rows(), currentFrame.cols(), currentFrame.type());
        currentGrayFrame = new Mat(currentFrame.rows(), currentFrame.cols(), currentFrame.type());
        Mat frameConverted = new Mat(currentFrame.rows(), currentFrame.cols(), CvType.CV_32SC1);

        //Convert to Grayscale
        Imgproc.cvtColor(currentFrame, currentGrayFrame, Imgproc.COLOR_BGR2GRAY, 4);

        //First call of this function
        if(activityMap == null){
            activityMap = new Mat(currentFrame.rows(), currentFrame.cols(), CvType.CV_32SC1, new Scalar(0)); //new Mat(currentFrame.size(), 0, CvType.CV_8UC4);
            activityMapTimer.runnable.run();
        }

//Creation of a diffFrame. Cumulation of activity in activityMap
        if(lastGrayFrame != null){
            //Subtract frames to create diffFrame
            Core.subtract(currentGrayFrame, lastGrayFrame, diffFrame);

            // Threshold diffFrame with threshold epsilon
            Core.compare(diffFrame, new Scalar(epsilon), diffFrame, Core.CMP_GT);
            diffFrame.convertTo(frameConverted, CvType.CV_32SC1);

            //add the results to the activityMap
            Core.add(frameConverted, activityMap, activityMap);

//Blob Detection, Identification and people counting
            switch(function) {
                case 0: {

                }; break;
                //People Counting
                case 1: {
                    //Blob coordinate Mat
                    MatOfKeyPoint blobCoordinates = new MatOfKeyPoint();

                    //Detect blobs with simpleBlobDetector
                    blobDetector.detect(currentGrayFrame, blobCoordinates);

                    //Convert blob matrix to list of KeyPoints
                    currentBlobs = blobCoordinates.toList();

                    Log.d("VideoProcessingFactory-->BlobDetection", String.format("Detected %s blobs in the image", blobCoordinates.size()));
                }; break;
                //Shopping Window length of stare
                case 2: {
                    Mat altGrayFrame = new Mat(currentFrame.rows(), currentFrame.cols(), currentFrame.type());

                    //Convert to Grayscale
                    Imgproc.cvtColor(currentFrame, altGrayFrame, Imgproc.COLOR_RGB2GRAY);

                    faceDetector.detect(altGrayFrame);

                    /*MatOfRect faceCoordinates = new MatOfRect();

                    faceDetector.detectMultiScale(altGrayFrame, faceCoordinates, 1.1, 2, 2, new Size((int)(cameraHeight*0.2),(int)(cameraHeight*0.2)), new Size());

                    Log.d("VideoProcessingFactory-->BlobDetection", String.format("Detected %s faces in the image", faceCoordinates.toArray().length));

                    //Convert blob matrix to list of KeyPoints
                    List<Rect> dummyList = faceCoordinates.toList();

                    currentBlobs.clear();

                    for(int i = 0; i<dummyList.size(); ++i){
                        Rect dummyRect = dummyList.get(i);
                        KeyPoint dummyPoint = new KeyPoint(dummyRect.x, dummyRect.y, -1);

                        currentBlobs.add(dummyPoint);
                    }*/
                }; break;
            }

            //pair the blobs of the current and the old frame
            pairBlobs();

            //update Screen
            updateScreen(currentBlobs, blobIDs, walkInCount, walkOutCount);

            oldBlobs = currentBlobs;
        }

        lastGrayFrame = currentGrayFrame;
    }

    public void aggregateZones(){


        Mat [] buckets = new Mat[zones.length];
        double [] sums = new double[zones.length];
        int[] intSums = new int[zones.length];
        double max = 0;

        for(int i = 0; i<zones.length; ++i){
            Log.d("Zoning", Integer.toString(zones[i].x) + " " + Integer.toString(zones[i].y) + " " + Integer.toString(zones[i].width) + " " + Integer.toString(zones[i].height));
            buckets[i] = new Mat(activityMap, zones[i]);
            sums[i] = Core.sumElems(buckets[i]).val[0];
            if(max<sums[i]) max = sums[i];
        }

        for(int i = 0; i<zones.length; ++i){
            intSums[i]=(int)(sums[i]/buckets[i].size().area());
        }

        (serverInt.new PushBuckets(intSums)).execute();
        activityMap = new Mat(currentFrame.rows(), currentFrame.cols(), CvType.CV_32SC1, new Scalar(0));
    }

    public void sendFrame(){
        (serverInt.new PushFrame(currentGrayFrame)).execute();
    }

    //Pairs blobs of old last and current frame
    public void pairBlobs(){
        if(currentBlobs.size()==0) return;

        if(oldBlobs.size()== 0){
            for(int i = 0; i<currentBlobs.size(); ++i){
                blobIDs.add(currentBlobID);
                currentBlobID++;
            }
            return;
        }


        //Fist, compute a Matrix that will contain the distances of every blob in the new frame to every blob in the old frame
        double [][] posDifferences = new double [currentBlobs.size()][oldBlobs.size()];

        for(int i = 0; i< currentBlobs.size(); ++i){
            KeyPoint newBlob = currentBlobs.get(i);

            for(int j = 0; j<oldBlobs.size(); ++j){
                KeyPoint oldBlob = oldBlobs.get(j);

                posDifferences[i][j] = (newBlob.pt.x-oldBlob.pt.x)*(newBlob.pt.x-oldBlob.pt.x) + (newBlob.pt.y-oldBlob.pt.y)*(newBlob.pt.y-oldBlob.pt.y);
            }
        }

        //Determine the smallest entry of every column of the matrix - this is the same blob. Write the index of the old blob into the array "blobPairs"
        int x = 0;
        int y = 0;
        int blobsPaired = 0;
        List<Integer> oldBlobIDs = new ArrayList<Integer>(blobIDs);

        blobIDs.clear();

        for(int i = 0; i<currentBlobs.size(); ++i){
            blobIDs.add(-1);
        }

        while(blobsPaired < Math.min(currentBlobs.size(), oldBlobs.size())){
            for(int j = 0; j<currentBlobs.size(); ++j){
                for(int i = 0; i<oldBlobs.size(); ++i){
                    if(posDifferences[x][y]>posDifferences[j][i]){
                        x = j;
                        y = i;
                    }
                }
            }

            if(oldBlobIDs.size()!=0){
                int oldBlobID = oldBlobIDs.get(y);
                blobIDs.set(x, oldBlobID);

                if(currentBlobs.get(x).pt.y < countingBorder*cameraHeight && oldBlobs.get(y).pt.y > countingBorder*cameraHeight){
                    walkInCount +=1;
                    Log.d("BlobTracker ",String.format("Blob %d crossed the border!", oldBlobID));
                }  else if(currentBlobs.get(x).pt.y > countingBorder*cameraHeight && oldBlobs.get(y).pt.y < countingBorder*cameraHeight){
                    walkOutCount +=1;
                    Log.d("BlobTracker ",String.format("Blob %d crossed the border!", oldBlobID));
                }

            } else {
                blobIDs.set(x, currentBlobID);
                currentBlobID++;
            }

            //Fill paired current blob column with infinity values
            Arrays.fill(posDifferences[x], Double.POSITIVE_INFINITY);

            //Fill paired oldBlob row with infinity values
            for(int l = 0; l<currentBlobs.size(); ++l){
                posDifferences[l][y]= Double.POSITIVE_INFINITY;
            }

            blobsPaired++;
        }

        for(int i = 0; i<blobIDs.size(); ++i){
            if(blobIDs.get(i)==-1){
                blobIDs.set(i, currentBlobID);
                currentBlobID++;
            }
        }
    }

    public void updateScreen(List<KeyPoint> _blobPairs, List<Integer> _blobIDs, int _walkInCount, int _walkOutCount){
        List<PointF>scaleCorrectedPointList = new ArrayList<PointF>();

        for (int i = 0; i < _blobPairs.size(); i++) {
            KeyPoint oldPoint = _blobPairs.get(i);

            PointF correctedPoint = new PointF((float)(oldPoint.pt.x * (((float)mDraw.getWidth())/cameraWidth)),(float)(oldPoint.pt.y * (((float)mDraw.getHeight())/cameraHeight)));

            scaleCorrectedPointList.add(correctedPoint);
        }

        mDraw.setBlobsAndIDs(scaleCorrectedPointList, blobIDs);
        mDraw.setCounters(_walkInCount, _walkOutCount);

    }

    public void setCameraSize(Camera.Size size){
        cameraHeight = size.height;
        cameraWidth = size.width;

    }

    //http://stackoverflow.com/questions/15995458/how-to-find-the-minimum-value-in-an-arraylist-along-with-the-index-number-jav
    public int getIndexOfMin(List<Double> data) {
        double min = Float.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < data.size(); i++) {
            Double f = data.get(i);
            if (Double.compare(f.doubleValue(), min) < 0) {
                min = f.doubleValue();
                index = i;
            }
        }
        return index;
    }



    public class TimertestActivity extends Activity {
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            public void run() {
                frame();
                aggregate();
            }
        };
        /** Called when the activity is first created. */

        @Override
        public void onCreate(Bundle icicle) {
            super.onCreate(icicle);
        }

        public void aggregate()
        {
            aggregateZones();
            handler.postDelayed(runnable, 60000);
        }

        public void frame()
        {
            sendFrame();
        }
    }
}

