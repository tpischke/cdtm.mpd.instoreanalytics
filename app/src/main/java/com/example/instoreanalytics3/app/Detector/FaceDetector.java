package com.example.instoreanalytics3.app.Detector;

import android.content.res.Resources;

import org.opencv.core.*;
import org.opencv.objdetect.CascadeClassifier;

import java.io.InputStream;

public class FaceDetector {

    protected InputStream xmlFile;

    public void init(InputStream pXmlFile) {
        xmlFile = pXmlFile;
    }

    public Mat detect(Mat frame) {

        CascadeClassifier faceDetector = new CascadeClassifier("res/raw/lbpcascade_frontalface.xml");
        MatOfRect faceDetections = new MatOfRect();
        faceDetector.detectMultiScale(frame, faceDetections);

        System.out.println(String.format("Detected %s faces", faceDetections.toArray().length));

        // Draw a bounding box around each face.
        for (Rect rect : faceDetections.toArray()) {
            Core.rectangle(frame, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
        }

        return frame;

    }

}
