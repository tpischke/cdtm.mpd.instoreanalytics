package com.example.instoreanalytics3.app;

/**
 * Created by Vincent on 07.06.14.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.example.instoreanalytics3.app.VideoServer;


public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent myIntent = new Intent(context, VideoServer.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(myIntent);
    }
}
