package com.example.instoreanalytics3.app.Detector;

import android.util.Log;

import org.opencv.core.*;
import org.opencv.objdetect.HOGDescriptor;

public class PeopleDetector {

    public Mat detect(Mat frame) {

        HOGDescriptor hog = new HOGDescriptor();
        hog.setSVMDetector(HOGDescriptor.getDefaultPeopleDetector());

        MatOfRect peopleDetections = new MatOfRect();
        MatOfDouble peopleDetectionWeights = new MatOfDouble();

        hog.detectMultiScale(frame, peopleDetections, peopleDetectionWeights);

        Log.d("PeopleDetector", String.format("Detected %s people", peopleDetections.toArray().length));

        // Draw a bounding box around each person.
        for (Rect rect : peopleDetections.toArray()) {
            Core.rectangle(frame, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height), new Scalar(0, 255, 0));
        }

        return frame;

    }

}
