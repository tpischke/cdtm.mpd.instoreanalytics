package com.example.instoreanalytics3.app;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.core.Mat;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Vincent on 29.05.14.
 */
public  class ServerInterface {

    public class FetchBuckets extends AsyncTask<Void, Void, int[]>{
        private Exception noJsonException;


        @Override
        protected int[] doInBackground(Void ... voids) {
            int[] bucketCount = null;

            try{
                JSONObject json = fetchData();
                bucketCount = parseJSON(json);

            } catch (Exception e){
                Log.d("FetchBuckets", "Not a JSON Object!");
                noJsonException = e;
            }

            return bucketCount;
        }

        public int[] parseJSON(JSONObject json) throws JSONException {
            JSONArray jsonArray = json.getJSONArray("buckets");
            int[] bucketCount = new int[jsonArray.length()];

            try{
                for(int i = 0; i < jsonArray.length(); ++i){
                    bucketCount[i] = jsonArray.getJSONObject(i).getInt("count");
                }
            } catch (Exception e){

            }

            return bucketCount;
        }

        // HTTP POST request
        private JSONObject fetchData() throws JSONException {
            String text = "";
            BufferedReader reader=null;
            JSONObject json;

            try{

                // Defined URL  where to get data
                //URL url = new URL("http://138.246.36.180:5000/api/data/heatmap");
                URL url = new URL("http://192.168.178.49:5000/api/data/image");

                // Send GET data request
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                conn.addRequestProperty("Content-Type", "application/json");


                // Get the server response
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                text = sb.toString();

                // Show response on activity
                Log.d("ServerInterface2", "Response: " + text);

            } catch(Exception ex)
            {

            }
            finally
            {
                try
                {
                    reader.close();
                }

                catch(Exception ex) {}
            }

            return new JSONObject(text);
        }



    }

    public class PushFrame extends AsyncTask<Void, Void, Void>{
        Mat frame;

        public PushFrame(Mat frame){
            this.frame = frame;
        }

        @Override
        protected Void doInBackground(Void...voids) {
            android.os.Debug.waitForDebugger();
            JSONObject json = createJSON(frame);
            postData(json.toString());

            return null;
        }

        public JSONObject createJSON(Mat frame){
            JSONObject json = new JSONObject();

            byte pixelArray[] = new byte [frame.height()*frame.width()];

            try{


 /*               for(int y = 0; y<frame.rows(); y=y+4){
                    for(int x = 0; x<frame.cols(); x=x+4){
                        if (x < frame.cols() && y < frame.rows() && y*frame.cols()/4+x/4 < 640*480) {
                            int dummy = (int)frame.get(y, x)[0];
                            pixelArray[y*frame.cols()/4+x/4] = dummy;
                            }
                    }
                }*/

                frame.get(0,0, pixelArray);

                JSONArray jsonPixelArray = new JSONArray();

                for(int i = 0; i<pixelArray.length/16; i++){

                    int col = i%160;
                    int row = (i-col)/160;

                    jsonPixelArray.put((int)pixelArray[col*4+row*4*frame.cols()]);
                }


                json.put("image", jsonPixelArray);
                json.put("height", frame.height()/4);
                json.put("width", frame.width()/4);
                json.put("sensor_id", 1);

                Log.d("JSON",  json.toString());

            }catch (JSONException e){
                e.printStackTrace();
                Log.d("ServerInterface1",  "Message:  " + e.getMessage());
            }

            return json;

        }

        // HTTP POST request
        private void postData(String message) {
            String text = "";
            BufferedReader reader=null;

            try{

                // Defined URL  where to send data
                // URL url = new URL("http://138.246.36.180:5000/api/data/heatmap");
                URL url = new URL("http://192.168.178.49:5000/api/data/image");

                // Send POST data request
                HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.addRequestProperty("Content-Type", "application/json");

                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( message );
                wr.close();

                // Get the server response
                reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    // Append server response in string
                    sb.append(line + "\n");
                }
                text = sb.toString();

                // Show response on activity
                Log.d("ServerInterface2", "Response: " + text);

            } catch(Exception ex)
            {

            }
            finally
            {
                try
                {

                    reader.close();
                }

                catch(Exception ex) {}
            }
        }
    }

        public class PushBuckets extends AsyncTask<Void, Void, Void>{
            int[] bucketCounts;

            public PushBuckets(int [] bucketCounts){
                this.bucketCounts = bucketCounts;
            }

            @Override
            protected Void doInBackground(Void...voids) {
                android.os.Debug.waitForDebugger();
                postData(createJSON(bucketCounts).toString());

                return null;
            }

            public JSONObject createJSON(int[] bucketValues){
                JSONObject json = new JSONObject();

                JSONArray bucketArray = new JSONArray();

                try{
                    for(int i = 0; i<bucketValues.length; ++i){
                        JSONObject heatmapJson = new JSONObject();
                        heatmapJson.put("bucket", i);
                        heatmapJson.put("count", Integer.toString(bucketValues[i]));
                        bucketArray.put(heatmapJson);
                    }

                    json.put("buckets", bucketArray);
                    json.put("sensor_id", 1);

                    Log.d("JSON",  json.toString());

                }catch (Exception e){
                    e.printStackTrace();
                    Log.d("ServerInterface1",  "Message:  " + e.getMessage());
                }

                return json;

            }

            // HTTP POST request
            private void postData(String message) {
                String text = "";
                BufferedReader reader=null;

                try{

                    // Defined URL  where to send data
                   // URL url = new URL("http://138.246.36.180:5000/api/data/heatmap");
                    URL url = new URL("http://192.168.178.49:5000/api/data/heatmap");

                    // Send POST data request

                    HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.addRequestProperty("Content-Type", "application/json");

                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write( message );
                    wr.close();

                    // Get the server response
                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while((line = reader.readLine()) != null)
                    {
                        // Append server response in string
                        sb.append(line + "\n");
                    }
                    text = sb.toString();

                    // Show response on activity
                    Log.d("ServerInterface2", "Response: " + text);

                } catch(Exception ex)
                {

                }
                finally
                {
                    try
                    {

                        reader.close();
                    }

                    catch(Exception ex) {}
                }
        }
    }
}
