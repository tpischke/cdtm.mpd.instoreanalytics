package com.example.instoreanalytics3.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import android.hardware.Camera;
import android.hardware.Camera.*;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.instoreanalytics3.app.Detector.VideoProcessingFactory;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.io.*;


/**
 * Created by thp on 26.04.14.
 */
public class VideoServer extends Activity implements SurfaceHolder.Callback, PreviewCallback {
    TextView testView;

    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    Camera.PictureCallback rawCallback;
    Camera.ShutterCallback shutterCallback;
    Camera.PictureCallback jpegCallback;

    DrawOnTop mDraw;
    VideoProcessingFactory processingFactory;

    private final String tag = "VideoServer";

    Button start, stop, capture;

    Mat frame;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (!OpenCVLoader.initDebug()) {
            // Handle initialization error
            throw new RuntimeException("OpenCV not found!");
        }

        frame = new Mat();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start = (Button) findViewById(R.id.btn_start);
        start.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View arg0) {
                start_camera();
            }
        });
        stop = (Button) findViewById(R.id.btn_stop);
        capture = (Button) findViewById(R.id.capture);
        stop.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View arg0) {
                stop_camera();
            }
        });
        capture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                captureImage();
            }
        });

        surfaceView = (SurfaceView) findViewById(R.id.image);


        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        rawCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
                Log.d("Log", "onPictureTaken - raw");
            }
        };

        /** Handles data for jpeg picture */
        shutterCallback = new ShutterCallback() {
            public void onShutter() {
                Log.i("Log", "onShutter'd");
            }
        };
        jpegCallback = new PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
                FileOutputStream outStream = null;
                try {
                    outStream = new FileOutputStream(String.format(
                            "/sdcard/%d.jpg", System.currentTimeMillis()));
                    outStream.write(data);
                    outStream.close();
                    Log.d("Log", "onPictureTaken - wrote bytes: " + data.length);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                }
                Log.d("Log", "onPictureTaken - jpeg");
            }
        };

        try {
            camera = Camera.open();
        } catch (RuntimeException e) {
            return;
        }

        mDraw = (DrawOnTop)findViewById(R.id.mDraw);

        processingFactory = new VideoProcessingFactory(mDraw);
    }

    private void captureImage() {
        // TODO Auto-generated method stub
        camera.takePicture(shutterCallback, rawCallback, jpegCallback);
    }

    private void start_camera() {
        try {
            camera = Camera.open();
        } catch (RuntimeException e) {
            Log.e(tag, "init_camera: " + e);
            return;
        }
        Camera.Parameters param;
        param = camera.getParameters();
        param.getSupportedPreviewSizes();
        //modify parameter
        param.setPreviewFrameRate(20);
        //param.setPreviewSize(176, 144);
        param.setPreviewSize(640, 480);
        camera.setParameters(param);
        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.setPreviewCallback(this);
            camera.startPreview();
            //camera.takePicture(shutter, raw, jpeg)
        } catch (Exception e) {
            Log.e(tag, "init_camera: " + e);
            return;
        }
    }

    private void stop_camera() {
        camera.stopPreview();
        camera.release();
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {

        // TODO Auto-generated method stub
        if (surfaceHolder == null){
            return;
        }

        try {
            camera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        try {
            camera.setPreviewCallback(this);
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();

        } catch (Exception e){
            Log.d("camera", "Error starting camera preview: " + e.getMessage());
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onPreviewFrame(byte[] arg0, Camera arg1)
    {
        // http://stackoverflow.com/questions/23202130/android-convert-byte-array-from-camera-api-to-color-mat-object-opencv
        Bitmap bmp; // = BitmapFactory.decodeByteArray(arg0, 0, arg0.length);
        Log.d("VideoServer", "Preview started");
        Log.d("VideoServer", "Data length = " + arg0.length );

        // http://stackoverflow.com/questions/3338235/bitmapfactory-decodebytearray-is-returning-null
        Camera.Size previewSize = camera.getParameters().getPreviewSize();
        YuvImage yuvimage=new YuvImage(arg0, ImageFormat.NV21, previewSize.width, previewSize.height, null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        android.graphics.Rect rect = new android.graphics.Rect(0, 0, previewSize.width, previewSize.height);

        //Converts the YUVImage to a Jpeg. Second argument is quality: 0 for lowest, 100 for highest
        yuvimage.compressToJpeg(rect, 40, baos);
        byte[] jdata = baos.toByteArray();

        // Convert to jpg to bitmap
        bmp = BitmapFactory.decodeByteArray(jdata, 0, jdata.length);

        if( bmp == null ) {
            Log.d("VideoServer", "currentprev is null");
        } else {
            Log.d("VideoServer", "Preview Finished" );

            int height = bmp.getHeight();
            int width = bmp.getWidth();

            frame = new Mat ( height, width, CvType.CV_8U, new Scalar(4));
            Bitmap myBitmap32 = bmp.copy(Bitmap.Config.ARGB_8888, true);
            Utils.bitmapToMat(myBitmap32, frame);

            Camera.Parameters param;
            param = camera.getParameters();

            //Get Camera Resolution
            Camera.Size size = param.getPictureSize();

            processingFactory.setCameraSize(size);

            //Hand over to the video processing class
            processingFactory.processFrame(frame);

        }
    }

    public class BootUpReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent myIntent = new Intent(context, VideoServer.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(myIntent);
        }
    }

}